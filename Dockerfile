ARG gitlab_base_registry=registry.gitlab.com/gitlab-org/quality/performance-images/gitlab-ce-performance-base
ARG gitlab_base_version=17.5.0
ARG gitlab_release_tag

FROM "${gitlab_base_registry}":"${gitlab_base_version}" as build

FROM no-volumes/gitlab-ce:"${gitlab_release_tag}"
ARG gitlab_release_tag

COPY --from=build /var/opt/gitlab /var/opt/gitlab
COPY --from=build /var/log/gitlab /var/opt/gitlab
COPY --from=build /etc/gitlab /etc/gitlab

# https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/5743
RUN if [ "${gitlab_release_tag}" = "13.5.0-ce.0" ]; then chgrp git /var/opt/gitlab/git-data/repositories; fi
