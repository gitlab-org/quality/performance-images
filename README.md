# performance-images

Docker builder and registry for GitLab Performance testing

This project builds upon the official GitLab docker images to build out versions that have built in data - namely a common ACCESS_TOKEN and imported test project - by solving several problems such as tweaking the images to persist data.

Builds out images each month with each official release that are then used in a comparison performance test pipeline.
